## Features

Provides Ansible Vault: Encode and Decode commands for windows, using a poweshell module.

## Requirements

https://github.com/jborean93/PowerShell-AnsibleVault
https://nodejs.org/en/

## Extension Settings

* `windows-vault.key`: Encryption key used for encode/decode commands

## Release Notes

### 0.0.1
Initial version

### 0.0.2
Add icon and repository