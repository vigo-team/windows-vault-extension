// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require('vscode');
const cp = require('child_process');

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed

/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {

	let decode = vscode.commands.registerCommand('extension.decode', function () {
		let currentlyOpenFile = vscode.window.activeTextEditor.document.fileName;
		let key = vscode.workspace.getConfiguration('windows-vault').get('key');
		let ps = cp.spawn("powershell.exe");
		ps.stdin.write(`Get-DecryptedAnsibleVault -Path '${currentlyOpenFile}' -Password (ConvertTo-SecureString -String ${key} -AsPlainText -Force) | Set-Content -Path '${currentlyOpenFile}' -NoNewLine\n`);
		ps.stderr.on("data",function(data){
			console.log("Powershell Errors: " + data);
		});
		ps.on("exit",function(){
			vscode.window.showInformationMessage('Decoded!');
		});
		ps.stdin.end();
	});
	context.subscriptions.push(decode);

	let encode = vscode.commands.registerCommand('extension.encode', function () {
		
		let currentlyOpenFile = vscode.window.activeTextEditor.document.fileName;
		let key = vscode.workspace.getConfiguration('windows-vault').get('key');
		let ps = cp.spawn("powershell.exe");
		ps.stdin.write(`Get-EncryptedAnsibleVault -Path '${currentlyOpenFile}' -Password (ConvertTo-SecureString -String ${key} -AsPlainText -Force) | Set-Content -Path '${currentlyOpenFile}' -NoNewLine\n`);
		ps.stderr.on("data",function(data){
			console.log("Powershell Errors: " + data);
		});
		ps.on("exit",function(){
			vscode.window.showInformationMessage('Encoded!');
		});
		ps.stdin.end();
	});
	context.subscriptions.push(encode);
}
exports.activate = activate;

// this method is called when your extension is deactivated
function deactivate() {}

module.exports = {
	activate,
	deactivate
}
